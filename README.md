# Name Distribution in Switzerland in the Last 100 Years

## Team Members

- **Zoe Vocat**
    - Email: [zoe.vocat@students.fhnw.ch](mailto:zoe.vocat@students.fhnw.ch)
- **Daniel Gugger**
    - Email: [daniel.gugger@student.fhnw.ch](mailto:daniel.gugger@student.fhnw.ch)
- **Claudine Christen**
    - Email: [claudine.christen@students.fhnw.ch](mailto:claudine.christen@students.fhnw.ch)

## Summary

In this project, we conduct a comprehensive analysis of first names given to newborns in Switzerland over the past century. Our study focuses on categorizing these names based on geographical factors, such as different Cantons and language regions, as well as by gender. The primary goal is to filter and analyze the data based on these categories to observe and understand naming trends across the country. Our visualizations aim to highlight the most popular names in each year, offering insights into the evolution of naming preferences over time.

## Data Sources

For our analysis, we have utilized the following primary data sources:

1. **Swiss First Names Open Data**: [https://vornamen.opendata.ch/](https://vornamen.opendata.ch/)
2. **Federal Statistical Office - Names of Newborns**: [https://www.bfs.admin.ch/bfs/de/home/statistiken/bevoelkerung/geburten-todesfaelle/vornamen-neugeborene.html](https://www.bfs.admin.ch/bfs/de/home/statistiken/bevoelkerung/geburten-todesfaelle/vornamen-neugeborene.html)
3. **Birth and Death Statistics**: [https://www.bfs.admin.ch/bfs/de/home/statistiken/bevoelkerung/geburten-todesfaelle/geburten.assetdetail.25945407.html](https://www.bfs.admin.ch/bfs/de/home/statistiken/bevoelkerung/geburten-todesfaelle/geburten.assetdetail.25945407.html)

## Information

This project is done for the module **Informations Visualization** at the **University of Applied Sciences and Arts Northwestern Switzerland**.

## Webpage: 
**https://ivis-project-naming-statistics-daniel-gugger-37a6521ada665b1175.pages.fhnw.ch/**
