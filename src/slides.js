const slides = document.querySelectorAll('.slide');
let currentSlide = 0;

const textElement1 = document.getElementById('animated-text-1');
const textElement2 = document.getElementById('animated-text-2');
const textElement3 = document.getElementById('animated-text-3');
const textElement4 = document.getElementById('animated-text-4');

const textToAnimate1 = 'Naming trends are subject to constant change and reflect the cultural, social and individual\n' +
    'influences of an era. Formerly popular names are rarely given years later.';

const textToAnimate2 = 'You may have noticed that the most popular first names 100 years ago were given much more frequently\n' +
    'than the most popular first names today. Let\'s take a look at the number of different names in comparison\n' +
    'to the birth rate over the years.';

const textToAnimate3 = 'Naming trends don\'t just change over the years. There are also different preferences in different regions. Now it\'s your turn. On the next slide choose a year and click on the canton for which you\'d like to see the most popular names.';

const textToAnimate4 = 'Use the slider, to see the distribution of a name changing over the years.';

showSlide(0);

function showSlide(index) {
    slides[currentSlide].style.display = 'none';
    slides[index].style.display = 'block';
    currentSlide = index;

    switch (currentSlide) {
        case 1:
            textElement1.textContent = "";
            animateText(textElement1, textToAnimate1);
            break;
        case 3:
            textElement2.textContent = "";
            animateText(textElement2, textToAnimate2);
            break;
        case 6:
            textElement3.textContent = "";
            animateText(textElement3, textToAnimate3);
            break;
        case 8:
            textElement4.textContent = "";
            animateText(textElement4, textToAnimate4);
            break;
    }
}
window.addEventListener('wheel', (event) => {
    if (Math.abs(event.deltaY) > 50) {
        debouncedSlideChange(event.deltaY);
    }
});


function debounce(func, delay) {
    let inDebounce;
    return function() {
        const context = this;
        const args = arguments;
        clearTimeout(inDebounce);
        inDebounce = setTimeout(() => func.apply(context, args), delay);
    }
}

const debouncedSlideChange = debounce(function(deltaY) {
    if (deltaY > 0) {
        if (currentSlide < slides.length - 1) {
            showSlide(currentSlide + 1);
        }
    } else {
        if (currentSlide > 0) {
            showSlide(currentSlide - 1);
        }
    }
}, 250); // 200ms delay


function animateText(element, text) {
    let index = 0;

    function animate() {
        if (index < text.length) {
            if(currentSlide === 1 || currentSlide === 3 || currentSlide === 6 || currentSlide === 8) {
                element.innerHTML += text.charAt(index);
                index++;
                setTimeout(animate, 20);
            }
        }
    }

    animate();
}
