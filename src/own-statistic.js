// Load your data

let ownStatisticData = [];
let selectedNames = [];
d3.csv("./../data/prenames.csv").then(function (csvData) {
    ownStatisticData = transformData(csvData);
    // Generate year options from the data
    populateYearOptions(ownStatisticData);
    // Generate name options from the data
    populateNameOptions(ownStatisticData);
    // Draw initial chart
    updateChart(ownStatisticData, 1922, 2022, []); // Default values
});

function transformData(csvData) {
    let transformedData = [];
    csvData.forEach(row => {
        Object.keys(row).forEach(key => {
            if (!isNaN(key) && key.length === 4) { // Ensure the key is a year
                transformedData.push({
                    name: row["Vorname"],
                    gender: row["Geschlecht"],
                    year: d3.timeParse("%Y")(key), // Parse the year as a Date object
                    amount: +row[key]
                });
            }
        });
    });
    return transformedData;
}

function populateYearOptions(data) {
    let ownStatisticYears = Array.from(new Set(data.map(d => d.year.getFullYear())))
        .sort()
        .filter(year => year >= 1922); // Filter to keep only years >= 1922

    let startYearSelect = document.getElementById('own-statistics-startYear');
    let endYearSelect = document.getElementById('own-statistics-endYear');

    ownStatisticYears.forEach(year => {
        startYearSelect.options[startYearSelect.options.length] = new Option(year, year);
        endYearSelect.options[endYearSelect.options.length] = new Option(year, year);
    });

    // Set the default values
    startYearSelect.value = ownStatisticYears[0]; // This will be 1922 or the first year >= 1922
    endYearSelect.value = ownStatisticYears[ownStatisticYears.length - 1]; // The latest year
}


function populateNameOptions(data) {
    let names = Array.from(new Set(data.map(d => d.name))).sort();

    var mySellect = sellect("#own-statistics-nameSelect", {
        originList: names,
        destinationList: [],
        onInsert: function(event, item) {
            selectedNames = mySellect.getSelected();
            updateChartBasedOnSelection(ownStatisticData);
        },
        onRemove: function( event, item ) {
            selectedNames = mySellect.getSelected();
            updateChartBasedOnSelection(ownStatisticData);
        },
        maxLimit: 6,
    });

    mySellect.init();
}

// Event listeners for the selectors
document.getElementById('own-statistics-startYear').addEventListener('change', () => updateChartBasedOnSelection(ownStatisticData));
document.getElementById('own-statistics-endYear').addEventListener('change', () => updateChartBasedOnSelection(ownStatisticData));

function updateChartBasedOnSelection(data) {
    let startYear = +document.getElementById('own-statistics-startYear').value;
    let endYear = +document.getElementById('own-statistics-endYear').value;

    updateChart(data, startYear, endYear, selectedNames);
}

function updateChart(data, startYear, endYear, selectedNames) {
    let startDate = new Date(startYear, 0, 1);
    let endDate = new Date(endYear, 0, 1);
    let filteredData = data.filter(d =>
        d.year >= startDate && d.year <= endDate &&
        selectedNames.includes(d.name)
    );

    buildOwnStatisticDiagram(filteredData, d3.select("#own-statistics-group-line-chart"));
}

function buildOwnStatisticDiagram(data, svgContainer) {
    // Clear existing contents
    svgContainer.selectAll("*").remove();

    // Set dimensions and margins of the graph
    const margin = { top: 10, right: 30, bottom: 30, left: 60 },
        width = 650 - margin.left - margin.right,
        height = 390 - margin.top - margin.bottom;

    // Append SVG object to the container
    const svg = svgContainer
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", `translate(${margin.left},${margin.top})`);

    // Add X axis
    const x = d3
        .scaleTime()
        .domain(d3.extent(data, d => d.year))
        .range([0, width]);
    svg.append("g")
        .attr("transform", `translate(0,${height})`)
        .call(d3.axisBottom(x).ticks(5));

    // Add Y axis
    const y = d3.scaleLinear()
        .domain([0, d3.max(data, d => +d.amount)])
        .range([height, 0]);
    svg.append("g")
        .call(d3.axisLeft(y));

    // Group the data: one array for each value of the X axis.
    const sumstat = d3.group(data, d => d.name);

    // Color palette
    const color = d3.scaleOrdinal()
        .domain(data.map(d => d.name))
        .range(d3.schemeSet2);

    // tooltip
    const tooltip = d3.select("#tooltip6").attr("class", "tooltip");

    // Draw the line
    sumstat.forEach((values, key) => {
        svg.append("path")
            .datum(values)
            .attr("fill", "none")
            .attr("stroke", color(key))
            .attr("stroke-width", 2)
            .attr("d", d3.line()
                .x(d => x(d.year))
                .y(d => y(+d.amount))
                .curve(d3.curveMonotoneX)
            )
            // events for tooltip
            .on("mouseover", function(event, d) {
                var xValue = x.invert(
                    event.pageX - svg.node().getBoundingClientRect().left
                );
                valueFromNameAndYear(d, xValue);

                tooltip.transition()
                    .duration(200)
                    .style("opacity", .9);
                tooltip.html(key + "<br/>" + "Occurence: " + tooltipValue)
                    .style("left", (event.pageX) + "px")
                    .style("top", (event.pageY - 28) + "px");
            })
            .on("mousemove", function(event, d) {
                tooltip.style("left", (event.pageX) + "px")
                    .style("top", (event.pageY - 28) + "px");
            })
            .on("mouseout", function(d) {
                tooltip.transition()
                    .duration(500)
                    .style("opacity", 0);
            });
    });
}