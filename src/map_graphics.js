// map_graphics.js

// Define your SVG dimensions
const width = 700, height = 400;

// Create the SVGs with a viewBox
const topNamesMap_svg = d3.select("#cantonTopNamesMap").append("svg")
    .attr("width", width)
    .attr("height", height);

const bubbleMap_svg = d3.select("#cantonBubbleMap").append("svg")
    .attr("width", width)
    .attr("height", height);

let selectedYearCanton = 2000;
let selectedYearBubble = 2000;
let selectedName = "Anna";
let selectedCantonId;
let selectedCantonName;
let geoData;
let scalingFactor = 100; // Adjust this to change the size of the bubbles

let nameData;  // Define nameData globally
let totalBirthsPerCanton; // Define totalBirthsPerCanton globally
let projection; // Define projection globally

// Create a tooltip
const tooltip = d3.select("body").append("div")
    .attr("class", "tooltip-bubbles");

// Function to resize SVGs
/*
function resize() {
    // Get the width of the parent container and calculate the SVG width
    let targetWidth = Math.min(parseInt(d3.select('#cantonTopNamesMap').style('width')), maxWidth);
    topNamesMap_svg.attr("width", targetWidth);
    topNamesMap_svg.attr("height", Math.round(targetWidth / (width / height)));

    targetWidth = Math.min(parseInt(d3.select('#cantonBubbleMap').style('width')), maxWidth);
    bubbleMap_svg.attr("width", targetWidth);
    bubbleMap_svg.attr("height", Math.round(targetWidth / (width / height)));
}
*/

// Load and process data
d3.json('./../data/swissBOUNDARIES3D_1_3_TLM_KANTONSGEBIET.geojson').then(geoJson => {
    d3.csv('./../data/firstnames.csv').then(data => {
        nameData = data;  // Assign the CSV data to the global variable
        geoData = geoJson; // Assign the GeoJSON data to the global variable
        projection = d3.geoMercator().fitSize([width, height], geoData);
        populateDropdown();
        drawCantonMap();
        drawBubbleMap()
    });
});

function populateDropdown() {
    let uniqueNames = new Set(nameData.map(d => d.Vorname));
    let dropdown = d3.select("#namesBubbleMap");
    uniqueNames.forEach(name => {
        dropdown.append("option").attr("value", name);
    });
}


function drawCantonMap() {
    const pathGenerator = d3.geoPath().projection(projection);

    topNamesMap_svg.selectAll(".canton")
        .data(geoData.features)
        .enter().append("path")
        .attr("class", "canton")
        .attr("d", pathGenerator)
        .attr("fill", d => cantonColors[d.properties.KANTONSNUM] || '#cccccc')
        .attr("opacity", 0.5)
        .on("click", function(event, d) {
            selectedCantonId = d.properties.KANTONSNUM;
            selectedCantonName = d.properties.NAME;
            showTopNamesForCanton();
        });

    // Assuming cantonShields are defined earlier
    topNamesMap_svg.selectAll(".canton-shield")
        .data(geoData.features)
        .enter().append("image")
        .attr("xlink:href", d => cantonShields[d.properties.KANTONSNUM])
        .attr("x", d => projection(getCantonCenter(d.properties.KANTONSNUM))[0] - 10)
        .attr("y", d => projection(getCantonCenter(d.properties.KANTONSNUM))[1] - 10)
        .attr("width", 20)
        .attr("height", 20);

    //resize();
}

function drawBubbleMap() {
    const pathGenerator = d3.geoPath().projection(projection);

    bubbleMap_svg.selectAll(".canton")
        .data(geoData.features)
        .enter().append("path")
        .attr("class", "cantonBubble")
        .attr("d", pathGenerator)
        .attr("fill", d => cantonColors[d.properties.KANTONSNUM] || '#cccccc')
        .attr("opacity", 0.5);

    updateBubbles();

    //resize();
}

function updateBubbles() {
    // Aggregate total births per canton
    totalBirthsPerCanton = nameData.reduce((acc, d) => {
        const cantonId = d['KantonId'];
        const count = +d[selectedYearBubble];
        acc[cantonId] = (acc[cantonId] || 0) + count;
        return acc;
    }, {});

    const filteredByName = nameData.filter(d => d.Vorname === selectedName);
    const yearData = transformCantonData(filteredByName, totalBirthsPerCanton, selectedYearBubble);

    const bubbles = bubbleMap_svg.selectAll(".canton")
        .data(yearData, d => d.cantonId);

    bubbles.enter()
        .append("circle")
        .attr("class", "cantonBubble canton")
        .attr("cx", d => projection(getCantonCenter(d.cantonId))[0])
        .attr("cy", d => projection(getCantonCenter(d.cantonId))[1])
        .attr("r", d => d.nameCount > 0 ? Math.sqrt(d.proportion) * scalingFactor : 0)
        .attr("fill", "#007bff") // Set the fill color, change as needed
        .attr("opacity", 0.6) // Set the opacity for semi-transparency
        .on("mouseover", mouseover)
        .on("mousemove", mousemove)
        .on("mouseout", mouseout);

    bubbles
        .attr("cx", d => projection(getCantonCenter(d.cantonId))[0])
        .attr("cy", d => projection(getCantonCenter(d.cantonId))[1])
        .attr("r", d => d.nameCount > 0 ? Math.sqrt(d.proportion) * scalingFactor : 0)
        .attr("fill", "#007bff") // Set the fill color, change as needed
        .attr("opacity", 0.6) // Set the opacity for semi-transparency
        .on("mouseover", mouseover)
        .on("mousemove", mousemove)
        .on("mouseout", mouseout);


    bubbles.exit().remove();
}

// Define event handler functions
function mouseover(event, d) {
    tooltip.html(`
        <strong>Canton:</strong> ${d.cantonName}<br>
        <strong>Total Births:</strong> ${totalBirthsPerCanton[d.cantonId]}<br>
        <strong>Name Count:</strong> ${d.nameCount}<br>
        <strong>Proportion:</strong> ${(d.proportion * 100).toFixed(2)}%
    `)
        .style("visibility", "visible");
}

function mousemove(event) {
    tooltip.style("top", (event.pageY - 10) + "px")
        .style("left", (event.pageX + 10) + "px");
}

function mouseout() {
    tooltip.style("visibility", "hidden");
}

function showTopNamesForCanton() {

    const shieldImage = cantonShields[selectedCantonId] || ''; // Fallback to an empty string if no shield is available
    d3.select("#topNamesShield").attr("src", shieldImage);
    // Filter data for the selected canton and year
    const namesForCantonAndYear = nameData.filter(d => +d.KantonId === selectedCantonId && d[selectedYearCanton]);

    // Separate lists for female and male names
    const femaleNames = namesForCantonAndYear.filter(d => d.Geschlecht === 'w');
    const maleNames = namesForCantonAndYear.filter(d => d.Geschlecht === 'm');

    // Function to find top names from a list
    const getTopNames = (names) => {
        return names
            .map(d => ({ name: d.Vorname, count: +d[selectedYearCanton] }))
            .sort((a, b) => b.count - a.count)
            .slice(0, 10)
            .map(d => `${d.name}: ${d.count}`)
            .join("<br>");
    };

    // Get top names for each gender
    const topFemaleNames = getTopNames(femaleNames);
    const topMaleNames = getTopNames(maleNames);

    // Display the top names for each gender
    d3.select("#topNamesList").html(`
        <h3>Top Female Names for ${selectedCantonName}</h3><div id="topFemaleNames" style="font-family: Inter, sans-serif;">
 ${topFemaleNames}
</div>
        <h3>Top Male Names for ${selectedCantonName}</h3><div id="topMaleNames" style="font-family: Inter, sans-serif;">
 ${topMaleNames}
</div>
    `);
}


// Update the map when the slider changes
d3.select("#yearSliderCanton").on("input", function() {
    selectedYearCanton = +d3.select(this).property("value");
    d3.select("#selectedYearCanton").text(`Selected Year: ${selectedYearCanton}`);

    if (selectedCantonId) {
        showTopNamesForCanton();
    } else {
        d3.select("#topNamesList").html('<h3>Choose a Canton</h3>');
    }
});

d3.select("#yearSliderBubble").on("input", function() {
    selectedYearBubble = +d3.select(this).property("value");
    d3.select("#selectedYearBubble").text(`Selected Year: ${selectedYearBubble}`);
    updateBubbles();
});

/*
document.getElementById('searchBubbleNameButton').addEventListener('click', function() {
    selectedName = document.getElementById('searchBubbleNameInput').value;
    d3.select("#selectedNameBubble").text(`Selected Name: ${selectedName}`);
    updateBubbles();
});
*/

d3.select("#nameDropdown").on("input", function() {
    selectedName = d3.select(this).property("value");
    d3.select("#selectedNameBubble").text(`Selected Name: ${selectedName}`);
    updateBubbles();
});

function transformCantonData(data, totalBirthsPerCanton, year) {
    // First, filter and map the data as before
    return data
        .filter(d => d['Sprachregion / Kanton'] !== 'Schweiz')
        .map(d => {
            const cantonId = d['KantonId'];
            const nameCount = +d[year];
            const totalBirths = totalBirthsPerCanton[cantonId];
            return {
                cantonId: cantonId,
                cantonName: d['Sprachregion / Kanton'],
                proportion: totalBirths > 0 ? nameCount / totalBirths : 0,
                nameCount: nameCount
            };
        })
        .filter(d => d.proportion > 0);


}


function getCantonCenter(canton) {
    // Define the center coordinates for each canton
    const cantonCenters = {
        1: [8.5417, 47.3769],  // Z�rich
        2: [7.4474, 46.9480],  // Bern / Berne
        3: [8.3093, 47.0502],  // Luzern
        4: [8.6480, 46.7719],  // Uri
        5: [8.6560, 47.0207],  // Schwyz
        6: [8.2710, 46.8772],  // Obwalden
        7: [8.3857, 46.9268],  // Nidwalden
        8: [9.0672, 47.0404],  // Glarus
        9: [8.5155, 47.1662],  // Zug
        10: [7.1618, 46.6817], // Fribourg / Freiburg
        11: [7.5326, 47.2079], // Solothurn
        12: [7.5886, 47.5596], // Basel-Stadt
        13: [7.7348, 47.4515], // Basel-Landschaft
        14: [8.6380, 47.6973], // Schaffhausen
        15: [9.2780, 47.3664], // Appenzell Ausserrhoden
        16: [9.4093, 47.3486], // Appenzell Innerrhoden
        17: [9.3767, 47.4235], // St. Gallen
        18: [9.5308, 46.6562], // Graub�nden / Grigioni / Grischun
        19: [8.2398, 47.3877], // Aargau
        20: [9.0710, 47.5682], // Thurgau
        21: [8.8017, 46.3317], // Ticino
        22: [6.6340, 46.5709], // Vaud
        23: [7.6276, 46.2096], // Valais / Wallis
        24: [6.9293, 46.9896], // Neuch�tel
        25: [6.1423, 46.2044], // Gen�ve
        26: [7.0734, 47.3440], // Jura
    };

    return cantonCenters[canton];
}

const cantonShields = {
    1: './../data/Wappen/Wappen_Zuerich.png', // Z�rich
    2: './../data/Wappen/Wappen_Bern.png', // Bern / Berne
    3: './../data/Wappen/Wappen_Luzern.png', // Luzern
    4: './../data/Wappen/Wappen_Uri_alt.svg.png', // Uri
    5: './../data/Wappen/Wappen_Schwyz_matt.svg.png', // Schwyz
    6: './../data/Wappen/Wappen_Obwalden_matt.svg.png', // Obwalden
    7: './../data/Wappen/Wappen_Nidwalden_matt.svg.png', // Nidwalden
    8: './../data/Wappen/1280px-Wappen_Glarus_matt.svg.png', // Glarus
    9: './../data/Wappen/Wappen_Zug_matt.svg.png', // Zug
    10: './../data/Wappen/Wappen_Freiburg_matt.svg.png', // Fribourg / Freiburg
    11: './../data/Wappen/Wappen_Solothurn_matt.svg.png', // Solothurn
    12: './../data/Wappen/Wappen_Basel-Stadt_matt.svg.png', // Basel-Stadt
    13: './../data/Wappen/Coat_of_arms_of_Kanton_Basel-Landschaft.svg.png', // Basel-Landschaft
    14: './../data/Wappen/Wappen_Schaffhausen_matt.svg.png',// Schaffhausen
    15: './../data/Wappen/Wappen_Appenzell_Ausserrhoden_matt.svg.png', // Appenzell Ausserrhoden
    16: './../data/Wappen/1024px-Wappen_Appenzell_Innerrhoden_matt.svg.png',// Appenzell Innerrhoden
    17: './../data/Wappen/Coat_of_arms_of_canton_of_St._Gallen.svg.png',// St. Gallen
    18: './../data/Wappen/Wappen_Graubuenden.png',// Graub�nden / Grigioni / Grisc
    19: './../data/Wappen/Wappen_Aargau_matt.svg.png',// Aargau
    20: './../data/Wappen/Wappen_Thurgau_matt.svg.png',// Thurgau
    21: './../data/Wappen/Wappen_Tessin_matt.svg.png',// Ticino
    22: './../data/Wappen/Wappen_Waadt_matt.svg.png',// Vaud
    23: './../data/Wappen/Wappen_Wallis_matt.svg.png',// Valais / Wallis
    24: './../data/Wappen/Wappen_Neuenburg_matt.svg.png',// Neuch�tel
    25: './../data/Wappen/Wappen_Genf_matt.svg.png',// Gen�ve
    26: './../data/Wappen/Wappen_Jura_matt.svg.png',// Jura
};

const cantonColors = {
    1: '#e6194B', // Z�rich
    2: '#3cb44b', // Bern
    3: '#4363d8', // Luzern
    4: '#f58231', // Uri
    5: '#911eb4', // Schwyz
    6: '#46f0f0', // Obwalden
    7: '#f032e6', // Nidwalden
    8: '#bcf60c', // Glarus
    9: '#fabebe', // Zug
    10: '#008080', // Fribourg
    11: '#e6beff', // Solothurn
    12: '#9A6324', // Basel-Stadt
    13: '#fffac8', // Basel-Landschaft
    14: '#800000', // Schaffhausen
    15: '#aaffc3', // Appenzell Ausserrhoden
    16: '#808000', // Appenzell Innerrhoden
    17: '#ffd8b1', // St. Gallen
    18: '#000075', // Graub�nden
    19: '#808080', // Aargau
    20: '#ffe119', // Thurgau
    21: '#fabebe', // Ticino
    22: '#ffd8b1', // Vaud
    23: '#a9a9a9', // Valais
    24: '#800000', // Neuch�tel
    25: '#808080', // Gen�ve
    26: '#aaffc3', // Jura
};

// Call resize function on window resize
//d3.select(window).on('resize', resize);

// Call resize function initially to set up the SVGs
//resize();


