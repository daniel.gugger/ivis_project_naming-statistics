let transformedBirthData = [];
let transformedNameData = [];

// dimensions
const marginBirthAndNames = { top: 50, right: 150, bottom: 50, left: 50 },
      widthBirthAndNames = 650 - marginBirthAndNames.left - marginBirthAndNames.right,
      heightBirthAndNames = 450 - marginBirthAndNames.top - marginBirthAndNames.bottom;

// append svg to index
const svg = d3
    .select("#birth-and-names")
    .append("svg")
    .attr("width", widthBirthAndNames + marginBirthAndNames.left + marginBirthAndNames.right)
    .attr("height", heightBirthAndNames + marginBirthAndNames.top + marginBirthAndNames.bottom)
    .append("g")
    .attr("transform", "translate(" + marginBirthAndNames.left + "," + marginBirthAndNames.top + ")");

Promise.all([
    d3.csv("./../data/birthratesSince1922.csv"),
    d3.csv("./../data/prenames.csv"),
]).then(function([birthrateData, namesData]) {
    // Transform birthrate data
    birthrateData.forEach(function(d) {
        let year = parseInt(d.Jahr);
        let birthrate = parseInt(d.Lebendgeburten.replace(/\s/g, ""));
        transformedBirthData.push({ year: year, amount: birthrate });
    });

    let uniqueNamesByYear = {};
    for (let year = 1922; year <= 2022; year++) {
        uniqueNamesByYear[year.toString()] = new Set();
    }

    // Count unique names per year
    namesData.forEach((row) => {
        for (let year = 1922; year <= 2022; year++) {
            let yearStr = year.toString();
            let count = parseInt(row[yearStr]);
            if (count > 0) {
                uniqueNamesByYear[yearStr].add(row["Vorname"]);
            }
        }
    });

    // Save the data in the array
    for (const year in uniqueNamesByYear) {
        transformedNameData.push({
            year: parseInt(year),
            amount: uniqueNamesByYear[year].size,
        });
    }

    // Build diagram after both datasets are processed
    buildDiagram(svg);
});

function buildDiagram(svg) {
    // Parse entries of year to date
    transformedNameData.forEach(function(d) {
        d.year = d3.timeParse("%Y")(d.year);
    });

    transformedBirthData.forEach(function(d) {
        d.year = d3.timeParse("%Y")(d.year);
    });

    // Avoid zeros in data for logarithmic scale
    transformedBirthData.forEach(function(d) {
        d.amount = Math.max(d.amount, 1);
    });
    
    transformedNameData.forEach(function(d) {
        d.amount = Math.max(d.amount, 1);
    });

    // Set the ranges
    const x = d3.scaleTime().range([0, widthBirthAndNames]);
    const y = d3.scaleLog().range([heightBirthAndNames, 0]).base(10).clamp(true);

    // Define the axes
    const xAxis = d3.axisBottom(x).ticks(5);
    const yAxis = d3.axisLeft(y).tickValues([300, 500, 1000, 1500, 2000, 50000, 80000, 120000]).tickFormat(d3.format(",.0f"));

    // Define the lines
    const birthLine = d3
        .line()
        .x(function(d) {
            return x(d.year);
        })
        .y(function(d) {
            return y(d.amount);
        })
        .curve(d3.curveMonotoneX);

    const nameLine = d3
        .line()
        .x(function(d) {
            return x(d.year);
        })
        .y(function(d) {
            return y(d.amount);
        })
        .curve(d3.curveMonotoneX);

    // Scale the range of the data
    x.domain(d3.extent(transformedBirthData, function(d) { return d.year; }));

    // Find the minimum and maximum for the Y-axis
    const minYValue = d3.min([...transformedBirthData, ...transformedNameData], d => d.amount);
    const maxYValue = d3.max([...transformedBirthData, ...transformedNameData], d => d.amount);
    y.domain([minYValue, maxYValue]);

    // path for birthrate
    svg
        .append("path")
        .data([transformedBirthData])
        .attr("class", "line")
        .attr("id", "birth")
        .attr("d", birthLine)
        .attr("stroke", "grey")
        .attr("stroke-width", "3px")
        .attr("fill", "none");

    // path for amount of names
    svg
        .append("path")
        .data([transformedNameData])
        .attr("class", "line")
        .attr("id", "names")
        .attr("d", nameLine)
        .attr("stroke", "#be55ce")
        .attr("stroke-width", "3px")
        .attr("fill", "none");

    // Add the X Axis
    svg
        .append("g")
        .attr("transform", "translate(0," + heightBirthAndNames + ")")
        .call(xAxis);

    // Add the Y Axis
    svg.append("g").call(yAxis);

    // Calculation of postion for labels
    const lastBirthDataPoint = transformedBirthData[transformedBirthData.length - 1];
    const lastBirthDataXPosition = x(lastBirthDataPoint.year);

    const lastNameDataPoint = transformedNameData[transformedNameData.length - 1];
    const lastNameDataXPosition = x(lastNameDataPoint.year);

    svg.append("text")
        .attr("x", lastBirthDataXPosition + 5)
        .attr("y", y(lastBirthDataPoint.amount))
        .attr("text-anchor", "start")
        .style("fill", "grey")
        .text("Birth rate")
        .style("font-family", "Inter, sans-serif");

    svg.append("text")
        .attr("x", lastNameDataXPosition + 5)
        .attr("y", y(lastNameDataPoint.amount))
        .attr("text-anchor", "start")
        .style("fill", "#be55ce")
        .text("Quantity of names")
        .style("font-family", "Inter, sans-serif");

    // tooltip
    const tooltip = d3.select("#tooltip2").attr("class", "tooltip");

    // Helper function for finding the closest year to show tooltip
    function findClosestData(year, dataset) {
        let closest = dataset[0];
        let closestDistance = Math.abs(year - closest.year.getFullYear());

        dataset.forEach(function (d) {
            let currentDistance = Math.abs(year - d.year.getFullYear());
            if (currentDistance < closestDistance) {
                closestDistance = currentDistance;
                closest = d;
            }
        });

        return closest;
    }

    // Add rect as hovering area for tooltip
    svg.append("rect")
        .attr("class", "overlay")
        .attr("width", widthBirthAndNames)
        .attr("height", heightBirthAndNames)
        .style("opacity", 0)
        .on("mouseover", mouseover)
        .on("mousemove", mousemove)
        .on("mouseout", mouseout);

    // Define the focus line
    const focusLine = svg.append("line")
        .attr("class", "focus-line")
        .style("stroke", "black")
        .style("stroke-width", 1)
        .style("stroke-dasharray", "3,3")
        .style("opacity", 0);

    // Events on helper rect in graph
    function mouseover() {
        focusLine.style("opacity", 1);
        tooltip.style("opacity", 1);
    }

    function mousemove(event) {
        let x0 = x.invert(d3.pointer(event)[0]);
        let year = x0.getFullYear();

        const rightEdge = x(new Date("2022-01-01"));
        if (d3.pointer(event)[0] >= rightEdge - 2) {
            year = 2022;
        }

        const closestBirthData = findClosestData(year, transformedBirthData);
        const closestNameData = findClosestData(year, transformedNameData);
        const difference = closestBirthData.amount - closestNameData.amount;

        focusLine
            .attr("x1", x(closestBirthData.year))
            .attr("y1", y(closestBirthData.amount))
            .attr("x2", x(closestNameData.year))
            .attr("y2", y(closestNameData.amount))
            .style("opacity", 1);

        tooltip
            .html(
                "Year: " + closestBirthData.year.getFullYear() +
                "<br/>Birth rate: " + closestBirthData.amount +
                "<br/>Quantity of names: " + closestNameData.amount +
                "<br/>" +
                "Difference: " +
                difference
            )
            .style("left", (event.pageX + 5) + "px")
            .style("top", (event.pageY - 28) + "px");
    }

    function mouseout() {
        focusLine.style("opacity", 0);
        tooltip.style("opacity", 0);
    }
}
