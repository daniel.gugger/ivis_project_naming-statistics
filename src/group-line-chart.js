let transformedData1 = [];
let transformedData2 = [];
let tooltipValue = 0;

// dimension settings
const margin = {top: 50, right: 50, bottom: 50, left: 50},
    widthGroupLineCharts = 550 - margin.left - margin.right,
    heightGroupLineCharts = 400 - margin.top - margin.bottom;

const svgWidth = widthGroupLineCharts + margin.left + margin.right + 150; // Added 150 more for the legend

// include svg in html
const svg1 = d3
    .select("#group-line-chart1")
    .append("svg")
    .attr("width", svgWidth)
    .attr("height", heightGroupLineCharts + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

const svg2 = d3
    .select("#group-line-chart2")
    .append("svg")
    .attr("width", svgWidth)
    .attr("height", heightGroupLineCharts + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

d3.csv("./../data/prenames.csv").then(function (data) {

    // take the top 3 of each gender for a given year
    const femaleNames = data.filter((d) => d.Geschlecht === "f");
    const maleNames = data.filter((d) => d.Geschlecht === "m");
    const getTopNames = (names, year) => {
        return names.sort((a, b) => b[year] - a[year]).slice(0, 3);
    };

    // top 3 per gender for 2000 (change as soon as older data is available)
    const topFemaleNames1 = getTopNames(femaleNames, "1922");
    const topMaleNames1 = getTopNames(maleNames, "1922");

    const topNamesCombined1 = topFemaleNames1.concat(topMaleNames1);

    // fill transformedData1 with top names for 2000 (*)
    topNamesCombined1.forEach((row) => {
        for (let year = 1922; year <= 2022; year++) {
            transformedData1.push({
                name: row.Vorname,
                year: year,
                amount: +row[year],
                gender: row.Geschlecht === "f" ? "f" : "m",
            });
        }
    });

    // top 3 per gender for 2022
    const topFemaleNames2 = getTopNames(femaleNames, "2022");
    const topMaleNames2 = getTopNames(maleNames, "2022");

    const topNamesCombined2 = topFemaleNames2.concat(topMaleNames2);

    // fill transformedData2 with top names for 2022
    topNamesCombined2.forEach((row) => {
        for (let year = 1922; year <= 2022; year++) {
            transformedData2.push({
                name: row.Vorname,
                year: year,
                amount: +row[year],
                gender: row.Geschlecht === "f" ? "f" : "m",
            });
        }
    });

    buildGroupLineChartDiagram(transformedData1, svg1);
    buildGroupLineChartDiagram(transformedData2, svg2);
});

function buildGroupLineChartDiagram(data, svg) {
    // parse entries of year to date
    data.forEach(function (d) {
        d.year = d3.timeParse("%Y")(d.year);
    });

    // group the data, so there can be one line per name
    var sumstat = d3.group(data, function (d) {
        return d.name;
    });

    // definition of x axis
    const x = d3
        .scaleTime()
        .domain(
            d3.extent(data, function (d) {
                return d.year;
            })
        )
        .range([0, widthGroupLineCharts]);
    svg
        .append("g")
        .attr("transform", "translate(0," + heightGroupLineCharts + ")")
        .call(d3.axisBottom(x).ticks(5));

    // definition of y axis
    const y = d3
        .scaleLinear()
        .domain([
            0,
            d3.max(data, function (d) {
                return +d.amount;
            }),
        ])
        .range([heightGroupLineCharts, 0]);
    svg.append("g").call(d3.axisLeft(y));

    // colors of lines
    const color = d3
        .scaleOrdinal()
        .domain(Array.from(sumstat.keys()))
        .range(["#ae1332", "#be55ce", "#f7b6d2", "#6f55ce", "#1f77b4", "#17becf"]);

    // line generator
    const lineGenerator = d3
        .line()
        .x(function (d) {
            return x(d.year);
        })
        .y(function (d) {
            return y(+d.amount);
        })
        .curve(d3.curveMonotoneX);

    // line id for toggling lines
    const lineId = function (name) {
        return "line-" + name.replace(/\s+/g, "");
    };

    // function for toggling lines
    const toggleLine = function (name) {
        // Toggle the line visibility
        let line = d3.select("#" + lineId(name));
        line.classed("hidden", !line.classed("hidden"));

        // Find the corresponding rectangle in the legend and toggle its fill
        d3.select(this.parentNode).select("rect")
            .style("fill", line.classed("hidden") ? "transparent" : color(name));
    };

    // draw lines
    svg
        .selectAll(".line")
        .data(Array.from(sumstat))
        .join("path")
        .attr("class", "line")
        .attr("id", function ([key, values]) {
            return lineId(key);
        }) // Add an id to each line
        .attr("fill", "none")
        .attr("stroke", function ([key, values]) {
            return color(key);
        })
        .attr("stroke-width", 2)
        .attr("d", function ([key, values]) {
            return lineGenerator(values);
        });

    // add legend
    const legendXPosition = widthGroupLineCharts + margin.right; // Adjust this as needed

    const legend = svg
        .selectAll(".legend")
        .data(sumstat.keys())
        .enter()
        .append("g")
        .attr("class", "legend")
        .attr("transform", function (d, i) {
            return "translate(" + legendXPosition + "," + (i * 25) + ")";
        });

    legend
        .append("text")
        .attr("x", 30)
        .attr("y", 15)
        .style("font-family", "Inter, sans-serif")
        .style("text-anchor", "start")
        .text(function (d) {
            return d;
        })
        .on("click", function (event, d) {
            toggleLine.call(this, d);
        });

    legend
        .append("rect")
        .attr("x", 5)
        .attr("width", 15)
        .attr("height", 15)
        .style("fill", function (d) {
            return color(d);
        })
        .style("stroke", function(d) {
            return color(d);
        })
        .style("stroke-width", 3)
        .on("click", function (event, d) {
            toggleLine.call(this, d);
        });

    // set html element as tooltip
    const tooltip = d3.select("#tooltip").attr("class", "tooltip");

    // add tooltip when hovering
    svg
        .selectAll(".line")
        .on("mouseover", function (event, d) {
            var xValue = x.invert(
                event.pageX - svg.node().getBoundingClientRect().left
            ); // Inverting the x-coordinate to get the year
            valueFromNameAndYear(d[1], xValue);

            tooltip.transition().duration(200).style("opacity", 0.9);
            tooltip
                .html(d[1][0].name + "<br/>" + "Occurence: " + tooltipValue)
                .style("left", event.pageX + "px")
                .style("top", event.pageY - 28 + "px");
        })
        .on("mouseout", function (d) {
            tooltip.transition().duration(500).style("opacity", 0);
        });

    const filterGender = function (gender) {
        svg.selectAll(".line")
            .each(function([key, values]) {
                // Determine if the current line should be hidden
                let isHidden = true;
                if (gender === "all" || gender === "" || gender === null) {
                    isHidden = false;
                } else {
                    isHidden = values[0].gender !== gender;
                }

                // Select the line and toggle its visibility
                d3.select(this).classed("hidden", isHidden);

                // Find the corresponding rectangle in the legend and update its fill
                const legendRect = svg.selectAll(".legend")
                    .filter(d => d === key)
                    .select("rect");
                legendRect.style("fill", isHidden ? "transparent" : color(key));
            });
    };


    const buttonGroup = svg
        .append("g")
        .attr("class", "button-group")
        .attr("transform", "translate(0, -40)");

// Filter button for male
    buttonGroup
        .append("rect")
        .attr("x", 0)
        .attr("y", 0)
        .attr("width", 90)
        .attr("height", 30)
        .style("fill", "lightblue")
        .on("click", function () {
            filterGender("m");
        });
    buttonGroup
        .append("text")
        .attr("x", 45)
        .attr("y", 20)
        .text("Male")
        .style("font-family", "Inter, sans-serif")
        .style("text-anchor", "middle")
        .on("click", function () {
            filterGender("m");
        });

// Filter button for female
    buttonGroup
        .append("rect")
        .attr("x", 100)
        .attr("y", 0)
        .attr("width", 90)
        .attr("height", 30)
        .style("fill", "pink")
        .on("click", function () {
            filterGender("f");
        });
    buttonGroup
        .append("text")
        .attr("x", 145)
        .attr("y", 20)
        .text("Female")
        .style("font-family", "Inter, sans-serif")
        .style("text-anchor", "middle")
        .on("click", function () {
            filterGender("f");
        });

// Reset button
    buttonGroup
        .append("rect")
        .attr("x", 200)
        .attr("y", 0)
        .attr("width", 90)
        .attr("height", 30)
        .style("fill", "lightgrey")
        .on("click", function () {
            filterGender("all");
        });
    buttonGroup
        .append("text")
        .attr("x", 245)
        .attr("y", 20)
        .text("Show all")
        .style("font-family", "Inter, sans-serif")
        .style("text-anchor", "middle")
        .on("click", function () {
            filterGender("all");
        });
}

function valueFromNameAndYear(data, year) {
    data.forEach((row) => {
        if (row.year.getFullYear() === year.getFullYear()) {
            tooltipValue = row.amount;
        }
    });
}
